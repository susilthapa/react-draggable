import React from "react";
import { Checkbox } from "antd";

const CheckBox = ({ item, handleInputChange }) => {
  console.log("CHECK = ", item);
  const handleChange = () => {};
  return item["options"] ? (
    <>
      <div style={{ color: "white" }}>
        <label>{item.label}</label>
      </div>
      <Checkbox.Group onChange={handleChange} options={item["options"]} />
    </>
  ) : (
    <Checkbox onChange={handleChange}>{item.label}</Checkbox>
  );
};

export default CheckBox;
