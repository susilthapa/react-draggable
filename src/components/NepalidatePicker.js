import React, { useContext } from "react";

import { NepaliDatePicker } from "nepali-datepicker-reactjs";
import "nepali-datepicker-reactjs/dist/index.css";

import { PositionContext } from "../contexts/PositionContext";

const NepalidatePicker = ({ item }) => {
  const { pat_data } = useContext(PositionContext);
  const [patientInfo, setPatientInfo] = pat_data;

  const handleChange = (val) => {
    console.log("Nepa Date = ", val, item.name);
    var adbs = require("ad-bs-converter");
    let enDate = adbs.bs2ad(val.replace(/-/g, "/"));
    let date = enDate.year + "-" + enDate.month + "-" + enDate.day;
    console.log("English Date = ", date);

    setPatientInfo({ ...setPatientInfo, [item.name]: date });
  };

  return (
    <NepaliDatePicker
      inputClassName="form-control nepali-date"
      // className="nepali-date"
      onChange={handleChange}
      options={{ calenderLocale: "ne", valueLocale: "en" }}
      name={item.name}
    />

    // <div>
    //   <select name="days" id="days">
    //     <option disabled selected>
    //       Day
    //     </option>
    //     <option value="saab">1</option>
    //     <option value="opel">2</option>
    //     <option value="audi">3</option>
    //   </select>
    //   <select name="months" id="monts">
    //     <option disabled selected>
    //       Month
    //     </option>
    //     <option value="volvo">1</option>
    //     <option value="saab">2</option>
    //     <option value="opel">3</option>
    //     <option value="audi">4</option>
    //   </select>
    //   <select name="years" id="years">
    //     <option disabled selected>
    //       Year
    //     </option>

    //     <option value="volvo">Volvo</option>
    //     <option value="saab">Saab</option>
    //     <option value="opel">Opel</option>
    //     <option value="audi">Audi</option>
    //   </select>
    // </div>
  );
};

export default NepalidatePicker;
