import React, { useContext, useState } from "react";

import { Form, Button } from "antd";
import "antd/dist/antd.css";

import { PositionContext } from "../contexts/PositionContext";
import "../css/Style.css";
import Draggable from "./Draggable";
import { updateRenderInfo } from "../utils";

const btn = {
  // position: "absolute",
  // bottom: "10px",
  // right: "10px",
  marginTop: "20px",
  width: "fit-content",
  alignSelf: "flex-end",
};

const PatientForm = (props) => {
  const { positions, pat_data } = useContext(PositionContext);
  const [fieldPositions, setFieldPositions] = positions;
  const [patientInfo] = pat_data;
  const render_info = fieldPositions["render_info"];

  const [edit, setEdit] = useState({
    value: true,
    text: "Enable Edit",
    color: "#1890ff",
  });
  // console.log('POSITIONS = ', fieldPositions)

  const getHeight = (fields) => {
    let heights = [];
    fields.map((field) =>
      heights.push(
        field["y_offset"] + parseInt(field["height"].replace("px", "")) + 20
      )
    );
    // console.log("Max height = ", Math.max(...heights));
    return Math.max(...heights);
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    console.log("Pat data = ", patientInfo);
    console.log("SUBMITTEDDDDDD");
    updateRenderInfo(fieldPositions.render_info); // updating only render_info
  };

  const handleClick = (e) => {
    edit.value
      ? setEdit({
          ...edit,
          value: false,
          text: "Disable Edit",
          color: "#DC143C",
        })
      : setEdit({
          ...edit,
          value: true,
          text: "Enable Edit",
          color: "#1890ff",
        });
  };

  return (
    <>
      <Form
        layout="vertical"
        style={{ marginTop: "25px" }}
        onSubmitCapture={(event) => handleFormSubmit(event)}
        autoComplete="off"
      >
        {/* <div className="form-rows"> */}
        <h1 style={{ textAlign: "center", color: "white" }}>
          {fieldPositions ? fieldPositions["form_name"] : null}
        </h1>
        <div className="main-row__btns">
          <Button
            type="primary"
            style={{ backgroundColor: edit.color }}
            onClick={handleClick}
            className="edit-btn"
          >
            {edit.text}
          </Button>
          {/* <Button type="primary" className="edit-btn">
                Sumbit
              </Button> */}
        </div>
        {fieldPositions.render_info
          ? render_info.map((note, i) => (
              <div id="field-row" key={i} className="main-row">
                <h2 style={{ fontWeight: "600", marginBottom: "0" }}>
                  {note.label}
                </h2>
                {note["notes"].map((item, index) => (
                  // console.log(index, y_offset_info[0]) &&
                  <div
                    className="fields__field_row"
                    style={{
                      height: getHeight(item["fields"]),
                      minHeight: 200,
                    }}
                    key={index}
                  >
                    <h3>{item["label"]}</h3>
                    <Draggable
                      key={index}
                      index={index}
                      noteIndex={i}
                      edit={edit.value}
                      noteName={note.name}
                      data={item["fields"]}
                    />
                  </div>
                ))}
              </div>
            ))
          : console.log("no Data Yet!")}
        {/* </div> */}
        <Button type="primary" style={btn} className="btn" htmlType="submit">
          Sumbit
        </Button>
      </Form>
    </>
  );
};

export default PatientForm;
