import React, { useState } from "react";
import { Input, Radio } from "antd";
import NepalidatePicker from "./NepalidatePicker";

const Date = ({ item, radioValue, handleRadio, handleInputChange }) => {
  return (
    <>
      <div style={{ minWidth: "250px" }}>
        <label style={{ color: "white" }}>{item.label}</label>
        <Radio.Group
          onChange={handleRadio}
          defaultValue="AD"
          style={{ marginLeft: "5px" }}
        >
          <Radio value="AD">AD</Radio>
          <Radio value="BS">BS</Radio>
        </Radio.Group>
      </div>

      {radioValue === "AD" ? (
        <input
          type={item.type}
          name={item.name}
          className="field-input"
          style={{ border: "none", outline: "none" }}
          onChange={handleInputChange}
        />
      ) : (
        <NepalidatePicker item={item} />
      )}
    </>
  );
};

export default Date;
