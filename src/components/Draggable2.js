import React, { useContext, useState } from "react";
import { Input, Radio } from "antd";

import { Rnd } from "react-rnd";
import { NepaliDatePicker } from "nepali-datepicker-reactjs";
import "nepali-datepicker-reactjs/dist/index.css";

import NepalidatePicker from "./NepalidatePicker";
import { PositionContext } from "../contexts/PositionContext";
import Dropdown from "./Dropdown";
import "../css/Style.css";

// import { PositionContext } from "../contexts/PositionContext";

let style = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "flex-end",
  // border: "solid",
  backgroundColor: "cadetblue",
  padding: "5px",
  // overflow: "ini",
  borderRadius: "5px",
  // margin: "5px",
};

const Draggable2 = ({ setPositions, data, id }) => {
  const { positions, pat_data } = useContext(PositionContext);
  const [fieldPositions, setFieldPositions] = positions;
  // const [patientInfo, setPatientInfo] = pat_data;

  const [radioValue, setRadioValue] = useState("AD");

  console.log("DRAGGABLE = ", data["fields"]);

  const handleChange = (e) => {
    console.log(e.target.name);
    // patientInfo[noteName] = {[e.target.name]: e.target.value}
    // setPatientInfo({
    //   ...patientInfo,
    //   [e.target.name]: e.target.value,
    // });
  };

  const handleRadio = (e) => {
    console.log("Radio", e.target.value);
    setRadioValue(e.target.value);
  };

  const renderInput = (item) => {
    switch (item.type) {
      // case "dropdown":
      //   return <Dropdown item={item} noteIndex={noteIndex} />;

      case "date":
        return (
          <>
            <div style={{ minWidth: "250px" }}>
              <label style={{ color: "white" }}>{item.label}</label>
              <Radio.Group
                onChange={handleRadio}
                defaultValue="AD"
                style={{ marginLeft: "5px" }}
              >
                <Radio value="AD">AD</Radio>
                <Radio value="BS">BS</Radio>
              </Radio.Group>
            </div>

            {radioValue === "AD" ? (
              <input
                type={item.type}
                name={item.name}
                className="field-input"
                style={{ border: "none", outline: "none" }}
                onChange={handleChange}
              />
            ) : (
              <NepalidatePicker item={item} />
            )}
          </>
        );
      default:
        return (
          <>
            <div style={{ color: "white" }}>
              <label>{item.label}</label>
            </div>
            <Input
              className="field-input"
              type={item.type}
              name={item.name}
              // value={patientInfo.name}
              onChange={handleChange}
              // placeholder={getFieldName(field.field_name)}
              autoComplete="off"
            />
          </>
        );
    }
  };

  return (
    <>
      {data["fields"].map((field, i) => (
        <Rnd
          key={i}
          bounds="parent"
          style={style}
          minWidth={200}
          minHeight={60}
          size={{ width: field.width, height: field.height }}
          position={{ x: field.x_offset, y: field.y_offset }}
          onDragStop={(e, d) => {
            field.x_offset = d.x;
            field.y_offset = d.y;
            // data["fields"][i] = field;
            // console.log("-----", data["fields"][i], field);
            setFieldPositions({ ...fieldPositions });

            setPositions(data, id);
          }}
          onResizeStop={(e, direction, ref, delta, position) => {
            field.x_offset = position.x;
            field.y_offset = position.y;
            field.width = ref.style.width;
            field.height = ref.style.height;
            // setFieldPositions({ ...fieldPositions });
          }}
        >
          {renderInput(field)}
        </Rnd>
      ))}
    </>
  );
};

export default Draggable2;
