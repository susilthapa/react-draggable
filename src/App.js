import React from "react";
import "./App.css";
import PatientForm from "./components/PatientForm";
import PatientForm2 from "./components/PatientForm2";
import PositionContextProvider from "./contexts/PositionContext";

function App() {
  return (
    <div className="App">
      <PositionContextProvider>
        <PatientForm />
      </PositionContextProvider>
    </div>
  );
}

export default App;
