import React, { useContext, useState } from "react";
import { Input } from "antd";

import { Rnd } from "react-rnd";
import "nepali-datepicker-reactjs/dist/index.css";

import { PositionContext } from "../contexts/PositionContext";
import Dropdown from "./Dropdown";
import Date from "./Date";
import CheckBox from "./CheckBox";

import "../css/Style.css";

let style = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  // border: "solid",
  backgroundColor: "cadetblue",
  padding: "5px",
  // overflow: "ini",
  borderRadius: "5px",
  // margin: "5px",
};

const Draggable = ({ data, index, noteIndex, edit, noteName }) => {
  const { positions, pat_data } = useContext(PositionContext);
  const [fieldPositions, setFieldPositions] = positions;
  const [patientInfo, setPatientInfo] = pat_data;

  // console.log("data, noteIndex, index  = ", data, noteIndex, index)

  const [radioValue, setRadioValue] = useState("AD");

  const handleInputChange = (e) => {
    // console.log(e.target.name);
    // patientInfo[noteName] = {[e.target.name]: e.target.value}
    setPatientInfo({
      ...patientInfo,
      [e.target.name]: e.target.value,
    });
  };

  // console.log('Data in drag = ', data)

  const handleRadio = (e) => {
    console.log("Radio", e.target.value);
    setRadioValue(e.target.value);
  };

  const renderInput = (item) => {
    switch (item.type) {
      case "dropdown":
        return <Dropdown item={item} noteIndex={noteIndex} />;

      case "date":
        return (
          <Date
            item={item}
            radioValue={radioValue}
            handleRadio={handleRadio}
            handleInputChange={handleInputChange}
          />
        );
      case "checkbox":
        return <CheckBox handleInputChange={handleInputChange} item={item} />;
      default:
        return (
          <>
            <div style={{ color: "white" }}>
              <label>{item.label}</label>
            </div>
            <Input
              className="field-input"
              type={item.type}
              name={item.name}
              // value={patientInfo.name}
              onChange={handleInputChange}
              // placeholder={getFieldName(field.field_name)}
              autoComplete="off"
            />
          </>
        );
    }
  };

  return (
    <>
      {data.map((item, i) => (
        <Rnd
          key={i}
          disableDragging={edit}
          enableResizing={!edit}
          bounds="parent"
          style={style}
          minWidth={200}
          minHeight={60}
          size={{ width: item.width, height: item.height }}
          // dragHandleClassName="handle"
          position={{ x: item.x_offset, y: item.y_offset }}
          onDragStart={(e, d) => {
            //  e.target.parentElement.style.opacity = "0.7";
          }}
          onDragStop={(e, d) => {
            // console.log(`X-Ofset = ${d.x}`);
            // console.log(`Y-Offset = ${d.y}`);
            e.target.parentElement.style.opacity = "1";
            item.x_offset = d.x;
            item.y_offset = d.y;
            // console.log("Name = ",fieldPositions['render_info'][index]['fields'][i])
            // console.log("Name2= ",item)
            // fieldPositions["render_info"][noteIndex]["notes"][index]["fields"][
            //   i
            // ] = item;

            setFieldPositions({ ...fieldPositions });
          }}
          onResizeStop={(e, direction, ref, delta, position) => {
            // console.log(`width = ${ref.style.width}`);
            // console.log(`height = ${ref.style.height}`);

            item.x_offset = position.x;
            item.y_offset = position.y;
            item.width = ref.style.width;
            item.height = ref.style.height;
            // fieldPositions["render_info"][noteIndex]["notes"][index]["fields"][
            //   i
            // ] = item;

            setFieldPositions({ ...fieldPositions });
          }}
          // onResize={(e, direction, ref, delta, position) => {
          //   field.width = ref.offsetWidth;
          //   field.height = ref.offsetHeight;
          //   setPositoins([...positions, field]);
          // }}
        >
          {renderInput(item)}
        </Rnd>
      ))}
    </>
  );
};

export default Draggable;
