import React from "react";

const Render = ({ note }) => {
  note.type === "group" ? <Render note={note.notes} /> : <h1>{note.label}</h1>;
};

export default Render;
