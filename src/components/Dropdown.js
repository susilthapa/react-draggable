import React, { useContext, useState } from "react";

import { Menu, Dropdown, Input } from "antd";
// import { DownOutlined } from "@ant-design/icons";
import {PositionContext} from '../contexts/PositionContext'
import "../css/DropDown.css";

const DropDown = ({item}) => {

  const {positions, pat_data } = useContext(PositionContext);
  const [patientInfo, setPatientInfo] = pat_data


//   const [fieldPositions, setFieldPositions] = positions

  
const handleSelect = (elem)=>{
    document.getElementById(item.name).value = elem
    // console.log('input value = ', elem)
    setPatientInfo({
        ...patientInfo,
        [item.name]: elem,
      });
}

const handleChange = (e) => {
    console.log('changed');
    setPatientInfo({
      ...patientInfo,
      [e.target.name]: e.target.value,
    });
  };

  const menu = (
    <Menu>
      {item.items.map((elem, i) => {
        return (
          <Menu.Item key={i} value={elem} onClick={()=>handleSelect(elem)}>
              {elem}
          </Menu.Item>
        );
      })}
    </Menu>
  );
  return (
    <>
      <div style={{color:"white"}}>
          <label>{item.label}</label>
      </div>
      <Dropdown overlay={menu} trigger={["click"]} className="dropdown">
          <input 
            id={item.name} 
            className="drp-input" 
            type="text" 
            name={item.name} 
            onChange={handleChange} 
            placeholder="Select..."/>
      </Dropdown>
    </>
  );
};

export default DropDown;
