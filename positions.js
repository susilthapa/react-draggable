data = [
  {
    type: "group",
    label: "Patient Informations",
    notes: [
      {
        type: "group",
        label: "Name",
        fields: [
          {
            name: "first_name",
            label: "First Name",
            type: "text",
            x_offset: 20,
            y_offset: 40,
            width: "200px",
            height: "60px",
          },
          {
            name: "middle_name",
            label: "Middle Name",
            type: "text",
            y_offset: 46,
            x_offset: 515,
            width: "201px",
            height: "60px",
          },
          {
            name: "last_name",
            label: "Last Name",
            type: "text",
            y_offset: 43,
            x_offset: 258,
            width: "200px",
            height: "60px",
          },
        ],
      },
      {
        type: "group",
        label: "Address",
        fields: [
          {
            name: "house_number",
            label: "House Number",
            type: "text",
            x_offset: 20,
            y_offset: 40,
            width: "200px",
            height: "60px",
          },
          {
            name: "street_number",
            label: "Street Number",
            type: "text",
            y_offset: 124,
            x_offset: 23,
            width: "200px",
            height: "60px",
          },
          {
            name: "postcode",
            label: "Postcode",
            type: "number",
            y_offset: 203,
            x_offset: 21,
            width: "200px",
            height: "60px",
          },
          {
            name: "district",
            label: "District",
            type: "dropdown",
            items: ["Gorkha", "Kathmandu", "Nuwakot", "Kaski", "Bhaktapur"],
            y_offset: 283,
            x_offset: 20,
            width: "200px",
            height: "60px",
          },
          {
            name: "country",
            label: "Country",
            type: "dropdown",
            items: [
              "Nepal",
              "India",
              "China",
              "America",
              "Australia",
              "Africa",
              "Brazil",
              "England",
            ],
            y_offset: 361,
            x_offset: 19,
            width: "200px",
            height: "60px",
          },
        ],
      },
      {
        type: "group",
        label: "Date",
        fields: [
          {
            name: "date_of_birth",
            label: "Date of Birth",
            type: "date",
            x_offset: 19,
            y_offset: 55,
            width: "253px",
            height: "62px",
          },
        ],
      },
    ],
  },
];

const rf = (ds) => {
  Object.keys(ds).map((k, v) => {
    if (typeof ds[k] === "object") {
      console.log(ds[k]);
      rf(ds[k]);
    } else {
      console.log("Values = ", k);
    }
  });
};

rf(data);
