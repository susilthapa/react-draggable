import React, { useState, createContext, useEffect } from "react";
import axios from "axios";

export const PositionContext = createContext();

const PositionContextProvider = (props) => {
  const [fieldPositions, setFieldPositions] = useState([]);
  const [patientInfo, setPatientInfo] = useState({});

  useEffect(() => {
    axios
      .get(`http://127.0.0.1:8000/rendering-info/`) // returns json not list of json
      .then((res) => {
        // console.log("Positions =", res.data);
        setFieldPositions(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <PositionContext.Provider
      value={{
        positions: [fieldPositions, setFieldPositions],
        pat_data: [patientInfo, setPatientInfo],
      }}
    >
      {props.children}
    </PositionContext.Provider>
  );
};

export default PositionContextProvider;
