import axios from 'axios'



export const updateRenderInfo = (info) =>{
    console.log("Updated = ", info)
    axios.put(`http://127.0.0.1:8000/rendering-info/update/`, info)
    .then((res)=>{
        console.log(res.data)
    })
    .catch(err=>console.log(err))
}