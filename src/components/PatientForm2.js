import React, { useContext, useState } from "react";

import { Form, Button } from "antd";
import "antd/dist/antd.css";

import { PositionContext } from "../contexts/PositionContext";
import "../css/Style.css";
import Draggable2 from "./Draggable2";
// import Render from "./Render";
import { updateRenderInfo } from "../utils";
import Draggable from "./Draggable2";
// import Render from "./Render";

const btn = {
  // position: "absolute",
  // bottom: "10px",
  // right: "10px",
  marginTop: "20px",
  width: "fit-content",
  alignSelf: "flex-end",
};

const PatientForm2 = (props) => {
  const { positions, pat_data } = useContext(PositionContext);
  const [fieldPositions, setFieldPositions] = positions;
  // const [patientInfo] = pat_data;
  const render_info = fieldPositions["render_info"];

  const [edit, setEdit] = useState({
    value: true,
    text: "Enable Edit",
    color: "#1890ff",
  });

  const getHeight = (fields) => {
    let heights = [];
    fields.map((field) =>
      heights.push(
        field["y_offset"] + parseInt(field["height"].replace("px", "")) + 20
      )
    );
    // console.log("HHHH = ", Math.max(...heights));
    return Math.max(...heights);
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    // console.log("Pat data = ", patientInfo);
    console.log("SUBMITTEDDDDDD");
    updateRenderInfo(fieldPositions.render_info); // updating only render_info
  };

  const handleClick = (e) => {
    edit.value
      ? setEdit({
          ...edit,
          value: false,
          text: "Disable Edit",
          color: "#DC143C",
        })
      : setEdit({
          ...edit,
          value: true,
          text: "Enable Edit",
          color: "#1890ff",
        });
  };

  const setPositions = (field, index, i) => {
    console.log("Updated Field = ", field, index, i);
    // setFieldPositions({
    //   ...fieldPositions,
    // });
    // const update = (info) =>
    //   info.map((data, i) => {
    //     data["fields"] ? console.log(data["fields"], i) : update(data["notes"]);
    //   });
    // update(render_info);
    // setFieldPositions
  };

  const Render = ({ info, id }) => (
    <>
      {info["fields"] ? (
        <div
          // key={i}
          className="fields__field_row"
          style={{ height: getHeight(info["fields"]), minHeight: 200 }}
        >
          <h3>{info["label"]}</h3>

          <Draggable2
            fields={info["fields"]}
            setPositions={setPositions}
            data={info}
            id={id}
          />
        </div>
      ) : (
        <>
          <h2 style={{ fontWeight: "600", marginBottom: "0" }}>{info.label}</h2>
          {info["notes"].map((notes, index) => (
            <Render info={notes} id={index} />
          ))}
        </>
      )}
    </>
  );

  return (
    <>
      <Form
        layout="vertical"
        style={{ marginTop: "25px" }}
        onSubmitCapture={(event) => handleFormSubmit(event)}
        autoComplete="off"
      >
        <h1 style={{ textAlign: "center", color: "white" }}>
          {fieldPositions ? fieldPositions["form_name"] : null}
        </h1>
        <div className="main-row__btns">
          <Button
            type="primary"
            style={{ backgroundColor: edit.color }}
            onClick={handleClick}
            className="edit-btn"
          >
            {edit.text}
          </Button>
        </div>
        {fieldPositions.render_info ? (
          <div id="field-row" className="main-row">
            {render_info.map((info, index) => (
              <Render info={info} id={index} />
            ))}
          </div>
        ) : (
          console.log("no Data Yet!")
        )}
        <Button type="primary" style={btn} className="btn" htmlType="submit">
          Sumbit
        </Button>
      </Form>
    </>
  );
};

export default PatientForm2;
